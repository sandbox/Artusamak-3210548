(function ($, Drupal) {

  'use strict';

  Drupal.searchableFacets = Drupal.searchableFacets || {};
  Drupal.behaviors.facetsCollapsibleGlobalFacetWidget = {
    attach: function (context) {
      Drupal.searchableFacets.collapseGlobalFacets(context);
    }
  };

  /**
   * Expose global facet blocks as searchable.
   */
  Drupal.searchableFacets.collapseGlobalFacets = function (context) {
    // Find all searchable facets that have children.
    var $facets = $('.js-facets-widget.js-searchable-global-facet', context)
      .once('facets-searchable-global-facets');

    if ($facets.length > 0) {
      $facets.each(function (index, facetBlock) {
        // Add a button to collapse the facet block.
        var $collapseButton = $('<span class="collapse-global-facet">-</span>');
        $collapseButton.on('click', function (e) {
          $(e.target).parents('div').toggleClass('collapsed');
        });
        $collapseButton.appendTo($(facetBlock).parent().find('h3'));
      });
    }

    //only on mobile
    if ($(window).width() <= 980) {
      //Catalog show filters on btn click
      const filterBtn = $('#catalog-filter-btn');
      if (filterBtn.length) {
        filterBtn.on('click', openFilter);
      }
    }
  };

  /**
  * Function called on catalog filter btn click
  *
  * add css class to filters element to show them
  *
  * @param event
  */
  const openFilter = (event) => {
    event.preventDefault();
    const filtersToShow = $('.block-facets');
    if (filtersToShow.length) {
      filtersToShow.addClass('open');
      $('#catalog-filter-btn').addClass('hidden');
      window.scrollTo(0,0);
    }
  };
})(jQuery, Drupal);
