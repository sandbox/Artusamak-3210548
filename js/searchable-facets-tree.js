(function ($, Drupal) {

  'use strict';

  Drupal.searchableFacets = Drupal.searchableFacets || {};
  Drupal.behaviors.facetsCollapsibleTreeFacetWidget = {
    attach: function (context) {
      Drupal.searchableFacets.collapseTree(context);
    }
  };

  /**
   * Turns all checkbox facet links that have children as searchable.
   */
  Drupal.searchableFacets.collapseTree = function (context) {
    // Find all checkbox facets that have children.
    var $facets = $('.js-searchable-facet-tree .facet-item--expanded', context)
      .once('facets-searchable-checkbox-transform');

    if ($facets.length > 0) {
      $facets.each(function (index, widget) {
        // Do not collapse a facet if it's part of the active trail.
        if (!$(widget).hasClass('facet-item--active-trail')) {
          $(widget).addClass('collapsed');
        }
        // Add a button to open / close the sub results.
        var $collapseButton = $('<span class="collapse-facet">-</span>');
        $collapseButton.on('click', function(e) {
          $(e.target).parent().toggleClass('collapsed');
        });
        $collapseButton.insertAfter($(widget).find('> label'));
      });
    }
  };

})(jQuery, Drupal);
