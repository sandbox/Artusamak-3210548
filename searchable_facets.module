<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function searchable_facets_form_facets_facet_edit_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $facet = $form_state->getFormObject()->getEntity();

  $form['facet_settings']['collapse_global_facet'] = [
    '#type' => 'checkbox',
    '#title' => 'Collapse this facet block.',
    '#description' => 'The block will be shown collapsed and users will be able to show or hide its content.',
    '#default_value' => $facet->getThirdPartySetting('searchable_facets', 'collapse_global_facet'),
  ];
  $form['facet_settings']['collapse_facet_tree'] = [
    '#type' => 'checkbox',
    '#title' => 'Show hierarchy as a searchable tree.',
    '#description' => 'A user will be able to collapse a tree level if your data are hierarchical.',
    '#default_value' => $facet->getThirdPartySetting('searchable_facets', 'collapse_facet_tree'),
  ];

  array_unshift($form['actions']['submit']['#submit'], 'searchable_facets_facet_settings_form_submit');
}

/**
 * Submit callback to store settings.
 */
function searchable_facets_facet_settings_form_submit($form, FormStateInterface $form_state) {
  $facet = $form_state->getFormObject()->getEntity();
  $facet->setThirdPartySetting('searchable_facets', 'collapse_global_facet', $form_state->getValue('facet_settings')['collapse_global_facet']);
  $facet->setThirdPartySetting('searchable_facets', 'collapse_facet_tree', $form_state->getValue('facet_settings')['collapse_facet_tree']);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function searchable_facets_preprocess_facets_item_list(&$variables) {
  if (!empty($variables['facet'])) {
    if ($variables['facet']->getThirdPartySetting('searchable_facets', 'collapse_global_facet')) {
      $variables['attributes']['class'][] = 'js-searchable-global-facet';
      $variables['#attached']['library'][] = 'searchable_facets/searchable_facets_global';
    }
    if ($variables['facet']->getThirdPartySetting('searchable_facets', 'collapse_facet_tree')) {
      $variables['attributes']['class'][] = 'js-searchable-facet-tree';
      $variables['#attached']['library'][] = 'searchable_facets/searchable_facets_tree';
    }
  }
}
